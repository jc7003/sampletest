package com.owen.sampletest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.owen.sampletest.R;
import com.owen.sampletest.tools.YouTubeExtractor;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class ParserPhPActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parser_ph_p);

        TextView textView = (TextView)findViewById(R.id.textView);

        try {
            YouTubeExtractor youTubeExtractor = new YouTubeExtractor("psBnG0ZvGmU");
            youTubeExtractor.startExtracting(new YouTubeExtractor.YouTubeExtractorListener() {
                @Override
                public void onSuccess(final YouTubeExtractor.YouTubeExtractorResult result) {
                    Log.d("ParserPhPActivity", "onSuccess url:" + result.getVideoUri().toString());
                    textView.setText(result.getVideoUri().toString());
                }

                @Override
                public void onFailure(Error error) {

                }
            });



        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private String readFile(){
        BufferedReader reader = null;
        StringBuffer br = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("text.txt")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                br.append( java.net.URLDecoder.decode(mLine, "UTF-8"));
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (reader != null)
                reader.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return br.toString();
    }

}
