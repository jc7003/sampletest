package com.owen.sampletest.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.owen.sampletest.R;
import com.owen.sampletest.tools.TextTool;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HyperLinkActivity extends AppCompatActivity {

    private static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hyperlink);
        mContext = this;

        TextView textView = (TextView)findViewById(R.id.textView);
        textView.setLinksClickable(true);

        textView.setText(new TextTool(this).getClickableHtml(readFile(), mOnClickListener));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private String readFile(){
        BufferedReader reader = null;
        StringBuffer br = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("htmlText")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                br.append( java.net.URLDecoder.decode(mLine, "UTF-8"));
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (reader != null)
                reader.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return br.toString();
    }

    private View.OnClickListener mOnClickListener = view -> {
        Log.d("mOnClickListener", "view.getTag():" + view.getTag());
        String url = (String)view.getTag();

        if(url.contains("\\\"")){
            url = url.replaceAll("\\\\\"", "");
        }
        gotoBrowser(HyperLinkActivity.this, url);
    };

    public void gotoBrowser(Context context, String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setDataAndType(Uri.parse(url), "text/html");
        browserIntent.addCategory(Intent.CATEGORY_BROWSABLE);

        try {
            context.startActivity(browserIntent);
        }
        catch (android.content.ActivityNotFoundException ex){
            ex.printStackTrace();
            Toast.makeText(context, "找不到瀏覽器，請先下載瀏覽器APP", Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
//            Crashlytics.logException(new Throwable("gotoBrowser error:" + e.toString()));
        }
    }
}
