package com.owen.sampletest.activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.owen.sampletest.R;
import com.owen.sampletest.adapter.ContentFragmentPageAdapter;

import java.util.ArrayList;

public class ViewpagerActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private ContentFragmentPageAdapter mAdapter;
    private ArrayList<String> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);
        mItems = new ArrayList<>();
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        FragmentManager fm = getSupportFragmentManager();

        mAdapter = new ContentFragmentPageAdapter(fm, mItems);
        mViewPager.setAdapter(mAdapter);

        initData();
    }

    private void initData(){
        mItems.add("test 1");
        mItems.add("test 2 youtube");
        mItems.add("test 3 brightcove");
        mItems.add("test 4");
        mItems.add("test 5");

        mAdapter.notifyDataSetChanged();
    }

}
