package com.owen.sampletest.activity;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.owen.sampletest.R;
import com.owen.sampletest.tools.ImageTools;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CreateViewByJsonActivity extends AppCompatActivity {

    private LinearLayout mRoot;
    private WebView mWebView;
    private ImageTools mImageTools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_craeteview);

        mRoot = (LinearLayout)findViewById(R.id.activity_main);
        mRoot.removeAllViews();
        mImageTools = new ImageTools(this);

        try{
            JSONObject jsonObject = new JSONObject(readJson());
            initView(jsonObject);
        }
        catch (Exception e){

        }
    }

    private String readJson(){
        BufferedReader reader = null;
        StringBuffer br = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("default.json")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                br.append(mLine);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (reader != null)
                reader.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return br.toString();
    }

    private void initView(JSONObject jsonObject) throws Exception{

        String title = jsonObject.getString("title");
        if(title!=null && title.length() > 0) {
            TextView titleView = new TextView(this);
            titleView.setText(title);
            titleView.setTextAppearance(this, android.R.style.TextAppearance_Large);
            mRoot.addView(titleView);
        }

        JSONArray content = jsonObject.getJSONArray("content");
        if(content != null) {
            for (int i = 0; i < content.length(); i++) {
                JSONArray itemArray = content.getJSONArray(i);
                if (itemArray != null) {
                    JSONObject object = itemArray.getJSONObject(0);

                    if(object.toString().contains("webview")) {
                        String webview = object.getString("webview");
                        Log.d("MainActivity", "webview:" + webview);
                        initWebView(webview);
                    }

                    if(object.toString().contains("text")) {
                        String text = object.getString("text");
                        Log.d("MainActivity", "text:" + text);
                        initText(text);
                    }

                    if(object.toString().contains("img")) {
                        String img = object.getString("img");
                        Log.d("MainActivity", "img:" + img);
                        initImage(img);
                    }
                }
            }
        }
    }

    private void initImage(String url){
        int width = getResources().getDisplayMetrics().widthPixels - ((int)getResources().getDimension(R.dimen.activity_vertical_margin) *2);
        int height = (width / 16) * 9;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);
        ImageView view = new ImageView(this);
        view.setLayoutParams(lp);

        mImageTools.getImageFormURL(url, null, view);

        mRoot.addView(view);
    }

    private void initText(String text){
        TextView view = new TextView(this);
        view.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        view.setText(text);

        mRoot.addView(view);
    }


    private void initWebView(String html){
        int width = getResources().getDisplayMetrics().widthPixels - ((int)getResources().getDimension(R.dimen.activity_vertical_margin) *2);
        int height = (width / 16) * 9;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);

        mWebView = new WebView(this);
        mWebView.setLayoutParams(lp);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);

        mWebView.requestFocus();
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(mWebChromeClient);
        mWebView.loadData(html, "text/html", "utf-8");
        mRoot.addView(mWebView);
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient(){

        private View mView = null;
        private CustomViewCallback mCallback = null;

        public void onProgressChanged(WebView view, int progress) {
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            if (mCallback != null) {
                mCallback.onCustomViewHidden();
                mCallback = null;
                return;
            }
            mWebView.setVisibility(View.GONE);

            ViewGroup parent = (ViewGroup) mWebView.getParent();
            parent.setBackgroundColor(Color.BLACK);
            parent.addView(view, 0);
            mView = view;
            mCallback = callback;

            setOrientation(true);
        }

        @Override
        public void onHideCustomView() {
            if (mView != null) {
                if (mCallback != null) {
                    mCallback.onCustomViewHidden();
                    mCallback = null;
                }

                ViewGroup parent = (ViewGroup) mView.getParent();
                parent.removeView(mView);
                mWebView.setVisibility(View.VISIBLE);
                mView = null;

                setOrientation(false);
            }
        }
    };

    private void setOrientation(boolean state){
        if(state){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
        else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}
