package com.owen.sampletest.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.owen.sampletest.R;
import com.owen.sampletest.restfull.RestClient;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        FormBody livebody = new FormBody.Builder().add("num_per_page", "35").build();
//        RestClient.get("https://tvbsapp-st.tvbs.com.tw/api/live_news", livebody, new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                Log.d("MainActivity", response.body().string());
//            }
//        });
//
//
//        RequestBody body = new FormBody.Builder().add("platform", "a").build();
//
//        RestClient.post("http://tvbsapp-st.tvbs.com.tw/api/getVersion", body, new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                Log.d("MainActivity", response.body().string());
//                Log.d("MainActivity", "code:" + response.code());
//            }
//        });

        init();
    }

    private void init(){
        Button ripple  = (Button)findViewById(R.id.ripple);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            ripple.setBackgroundResource(R.drawable.ripple_sample);
        else
            ripple.setBackgroundResource(android.R.drawable.btn_default);
    }

    public void onClick(View view){
        Intent intent = null;
        switch (view.getId()){
            case R.id.CreateViewByJson:
                intent = new Intent(this, CreateViewByJsonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.SearchView:
                intent = new Intent(this, SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.ParserPHP:
                intent = new Intent(this, ParserPhPActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.pick_number:
                intent = new Intent(this, PickNumbersActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.hyperlink:
                intent = new Intent(this, HyperLinkActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.viewpager:
                intent = new Intent(this, ViewpagerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.bottom_navigation:
                intent = new Intent(this, BtmNavigationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }
}
