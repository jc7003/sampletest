package com.owen.sampletest.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.owen.sampletest.R;

import java.util.ArrayList;

public class PickNumbersActivity extends AppCompatActivity {


    private String[] mLocation = {"台北市","台中市","高雄市", "花蓮市"};
    private String[] mAreaLocation = {"全部", "北部", "中部", "南部", "東部"};
    private String[] mDefault = {"全部", "北部", "中部", "南部", "東部"};
    private String[] mNorthLocation = {"基隆市", "臺北市", "新北市", "桃園市", "新竹市", "新竹縣", "苗栗縣"};
    private String[] mCentralLocation = {"臺中市", "彰化縣", "南投縣", "雲林縣", "嘉義市", "嘉義縣"};
    private String[] mSouthLocation = {"臺南市", "高雄市", "屏東縣"};
    private String[] mEastLocation = {"臺東縣", "花蓮縣", "宜蘭縣"};
    private ArrayList<String> mAllLocation = new ArrayList();

    private NumberPicker mLocalDistrictPicker;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_numbers);
        mTextView = (TextView)findViewById(R.id.textView2);

        addLocalToList(mNorthLocation);
        addLocalToList(mCentralLocation);
        addLocalToList(mSouthLocation);
        addLocalToList(mEastLocation);
    }

    private void addLocalToList(String[] array){
        for(String local : array)
            mAllLocation.add(local);
    }

    public void onClick(View view){
        showLocalDialog();
    }

    private void showLocalDialog(){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.dailog_locallist, null);

        //設定地點
        String[] local = mAllLocation.toArray(new String[0]);
        mLocalDistrictPicker = (NumberPicker)view.findViewById(R.id.local_district);
        initNumberPicker(mLocalDistrictPicker, local);

        //設定區域
        NumberPicker area = (NumberPicker)view.findViewById(R.id.local_area);
        initNumberPicker(area, mAreaLocation);

        area.setOnValueChangedListener(mAreaOnValueChangeListener);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setTitle("請選擇地點");
        builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("AlertDialog", "area.getValue():" + mAreaLocation[area.getValue()]);
                mTextView.setText("地點： " + getLocationArray(area.getValue())[mLocalDistrictPicker.getValue()]);
            }
        });

        builder.create().show();
    }

    private void initNumberPicker(NumberPicker numberPicker, final String[] data){
        numberPicker.setValue(0);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(data.length-1);
        numberPicker.setDisplayedValues(data);
        numberPicker.setWrapSelectorWheel(true);
        numberPicker.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                return data[i];
            }
        });

        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    }

    private void updateNumberPicker(NumberPicker numberPicker, final String[] data){
        int min = 0;
        int max = data.length - 1;
        int maxV = numberPicker.getMaxValue();
        if (max> maxV){
            numberPicker.setMinValue(min);
            numberPicker.setValue(0);
            numberPicker.setDisplayedValues(data);
            numberPicker.setMaxValue(max);
        }else{
            numberPicker.setMinValue(min);
            numberPicker.setValue(0);
            numberPicker.setMaxValue(max);
            numberPicker.setDisplayedValues(data);
        }

        numberPicker.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                return data[i];
            }
        });
    }

    private String[] getLocationArray(int index){
        String[] data = {"null"};
        switch (index){
            case 0:
                data = mAllLocation.toArray(new String[0]);
                break;
            case 1:
                data = mNorthLocation;
                break;
            case 2:
                data = mCentralLocation;
                break;
            case 3:
                data = mSouthLocation;
                break;
            case 4:
                data = mEastLocation;
                break;
        }

        return data;
    }

    private NumberPicker.OnValueChangeListener mAreaOnValueChangeListener = new NumberPicker.OnValueChangeListener(){

        @Override
        public void onValueChange(NumberPicker numberPicker, int i, int i1) {
            updateNumberPicker(mLocalDistrictPicker, getLocationArray(i1));
        }
    };
}
