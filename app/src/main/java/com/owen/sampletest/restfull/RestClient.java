package com.owen.sampletest.restfull;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by OwenChen on 2017/2/23.
 */

public class RestClient {

    private static final String GET = "GET";
    private static final String POST = "POST";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void get(String url, Callback callback){
        OkHttpClient okHttpClient = new OkHttpClient();
        Request.Builder requestBuilder = new Request.Builder().url(url);
        requestBuilder.method(GET, null);
        Request request = requestBuilder.build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
    }

    public static void get(String url, FormBody formBody, Callback callback){
        try {
            StringBuilder result = new StringBuilder();
            result.append(url);
            result.append(getQuery(formBody));

            OkHttpClient okHttpClient = new OkHttpClient();
            Request.Builder requestBuilder = new Request.Builder().url(result.toString());
            requestBuilder.method(GET, null);
            Request request = requestBuilder.build();
            Call call = okHttpClient.newCall(request);
            call.enqueue(callback);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void post(String url, RequestBody formBody, Callback callback){
        OkHttpClient okHttpClient = new OkHttpClient();
        Request.Builder requestBuilder = new Request.Builder().url(url);
        requestBuilder.method(POST, formBody);
        Request request = requestBuilder.build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
    }

    private static String getQuery(FormBody formBody) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        result.append("?");
        boolean first = true;

        for(int i =0; i< formBody.size(); i++){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(formBody.name(i), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(formBody.value(i), "UTF-8"));
        }

        return result.toString();
    }
}
