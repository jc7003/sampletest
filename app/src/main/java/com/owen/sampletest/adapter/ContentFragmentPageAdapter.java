package com.owen.sampletest.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.owen.sampletest.fragment.TestFragment;

import java.util.List;

/**
 * Created by TVBS on 2016/11/30.
 */

public class ContentFragmentPageAdapter extends FragmentStatePagerAdapter {

    private List<String> mItems;

    public ContentFragmentPageAdapter(FragmentManager fm, List<String> items) {
        super(fm);
        mItems = items;
    }

    @Override
    public Fragment getItem(int position) {
        TestFragment fragment = TestFragment.newInstance();
        fragment.setData(mItems.get(position));

        return fragment;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }
}
