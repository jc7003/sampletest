package com.owen.sampletest.fragment;

import android.util.Log;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.owen.sampletest.R;

/**
 * Created by owenchen on 2016/5/20.
 */
public class YoutubeFragment extends YouTubePlayerSupportFragment {

    private static final int RECOVERY_REQUEST = 1;
    public static String mUrl;

    private YouTubePlayer mActivePlayer;
    private boolean mIsFullScreen = false;

    public void setUrl(String url){
        mUrl = url;
    }

    public static YoutubeFragment newInstance() {
        Log.d("YoutubeFragment", "newInstance");
        YoutubeFragment playerYouTubeFrag = new YoutubeFragment();

        return playerYouTubeFrag;
    }

    public void init() {

        initialize("AIzaSyAHJtPJ1ZT0Iq0IZj3tu6tcICKN8Oee3ds", new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) { }

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                mActivePlayer = player;
                mActivePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                if (!wasRestored) {
                    mActivePlayer.cueVideo(mUrl, 0);
                }

                mActivePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                    @Override
                    public void onFullscreen(boolean b) {
                        mIsFullScreen = b;                    }
                });

            }
        });
    }

    public void onYouTubeVideoPaused() {
        mActivePlayer.pause();
    }

    public void setFullScreen(boolean state){
        mActivePlayer.setFullscreen(state);
    }

    public boolean getFullScreen(){
        return mIsFullScreen;
    }

    public void onResume(){
        if(mActivePlayer != null)
            mActivePlayer.play();
        super.onResume();
    }

    public void onPause(){
        if(mActivePlayer != null)
            mActivePlayer.pause();
        super.onPause();
    }
}
