package com.owen.sampletest.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.VideoListener;
import com.brightcove.player.event.Event;
import com.brightcove.player.event.EventEmitter;
import com.brightcove.player.event.EventListener;
import com.brightcove.player.event.EventType;
import com.brightcove.player.model.Video;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;
import com.owen.sampletest.R;

/**
 * Created by owenchen on 2016/5/20.
 */
public class BrightCoveFragment extends Fragment {

    protected BrightcoveExoPlayerVideoView mBrightcoveVideoView;
    private Catalog mCatalog;
    protected static OnFullScreenListener mFragmentOnFullScreenListener;
    private static String mID;

    public interface OnFullScreenListener{
        void fullscreen(boolean state);
    }

    public void setVideoID(String id){
        mID = id;
    }

    public static BrightCoveFragment newInstance() {
        Log.d("BrightCoveFragment", "newInstance");
        BrightCoveFragment f = new BrightCoveFragment();

        return f;
    }

    public void setOnFullScreenListener(OnFullScreenListener listener){
        mFragmentOnFullScreenListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("BrightCoveFragment", "onCreateView");
        View view = inflater.inflate(R.layout.video_brightcove_layout, container, false);

        mBrightcoveVideoView = (BrightcoveExoPlayerVideoView) view.findViewById(R.id.brightcove_video_view);

        final EventEmitter eventEmitter = mBrightcoveVideoView.getEventEmitter();
        mCatalog = new Catalog(eventEmitter, getString(R.string.tvbs_account), getString(R.string.tvbs_policy));
        mCatalog.findVideoByID(mID, mVideoListener);

        mBrightcoveVideoView.getEventEmitter().on(EventType.ENTER_FULL_SCREEN, new EventListener() {
            @Override
            public void processEvent(Event event) {
                mFragmentOnFullScreenListener.fullscreen(true);
            }
        });

        mBrightcoveVideoView.getEventEmitter().on(EventType.EXIT_FULL_SCREEN, new EventListener() {
            @Override
            public void processEvent(Event event) {
                mFragmentOnFullScreenListener.fullscreen(false);
            }
        });

        return view;
    }

    private void playVideo(Video video) {
        mBrightcoveVideoView.add(video);
    }

    @Override
    public void onResume(){
//        if(mBrightcoveVideoView != null)
//            mBrightcoveVideoView.start();

        super.onResume();
    }

    @Override
    public void onPause(){
        if(mBrightcoveVideoView != null)
            mBrightcoveVideoView.pause();

        super.onPause();
    }

    private void printInfo(Video video) {
//        L.d("BrightCoveFragmet", "name: " + video.getProperties().get("name"));
//        L.d("BrightCoveFragmet", "id: " + video.getProperties().get("id"));
//        L.d("BrightCoveFragmet", "referenceId: " + video.getProperties().get("referenceId"));
//        L.d("BrightCoveFragmet", "shortDescription: " + video.getProperties().get("shortDescription"));
//        L.d("BrightCoveFragmet", "stillImageUri: " + video.getProperties().get("stillImageUri"));
//        L.d("BrightCoveFragmet", "duration: " + video.getProperties().get("duration"));
//        L.d("BrightCoveFragmet", "customFields: " + video.getProperties().get("customFields"));
//        L.d("BrightCoveFragmet", "pubId: " + video.getProperties().get("pubId"));
//        L.d("BrightCoveFragmet", "headers: " + video.getProperties().get("headers"));
//        L.d("BrightCoveFragmet", "contentId: " + video.getProperties().get("contentId"));
//        L.d("BrightCoveFragmet", "captionSources: " + video.getProperties().get("captionSources"));
//        L.d("BrightCoveFragmet", "subtitleSources: " + video.getProperties().get("subtitleSources"));
//        L.d("BrightCoveFragmet", "captioning: " + video.getProperties().get("captioning"));
    }

    protected VideoListener mVideoListener = new VideoListener() {
        @Override
        public void onVideo(Video video) {
//            L.d("BrightCoveFragmet", "onVideo: video = " + video);

            playVideo(video);
        }
    };

}
