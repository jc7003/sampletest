package com.owen.sampletest.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.owen.sampletest.R;

/**
 * Created by TVBS on 2016/11/30.
 */

public class TestFragment extends Fragment{

    private String mData;
//    private LinearLayout mContentLayout;
    private LinearLayout mRoot;
//    private FrameLayout mFrameLayout;
    private Fragment mCurrentFragment;
    private String VIDEO_TAG = "VIDEO_TAG";

    private enum  VIDEO_TYPE {Youtube, BrightCove}

    public static TestFragment newInstance() {
        TestFragment f = new TestFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        Log.d("TestFragment", "onCreateView");
        mRoot = (LinearLayout)inflater.inflate(R.layout.fragment_layout, container, false);
//        mContentLayout = (LinearLayout)view.findViewById(R.id.content_layout);
//        mFrameLayout =  (FrameLayout)view.findViewById(R.id.frame_layout);

        initVideo(mData);
        initTitle(mData);
        return mRoot;
    }

    public void setData(String data){
        Log.d("TestFragment", "setData data:" +data);
        mData = data;
    }

    private void initTitle(String title){
        TextView textView = new TextView(getContext());
        textView.setTextAppearance(getContext(), android.R.style.TextAppearance_Large);
        textView.setText(title);

        mRoot.addView(textView);
    }

    private void initVideo(String data){
        int width = getContext().getResources().getDisplayMetrics().widthPixels;
        int height = width / 16 * 9;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setTag(View.generateViewId());
        linearLayout.setId((int)linearLayout.getTag());
        linearLayout.setLayoutParams(lp);

        if(data.contains("youtube")) {
            mRoot.addView(linearLayout);
            changeFragment(VIDEO_TYPE.Youtube, (int)linearLayout.getTag());
        }

        if(data.contains("brightcove")) {
            mRoot.addView(linearLayout);
            changeFragment(VIDEO_TYPE.BrightCove, (int)linearLayout.getTag());
        }

    }

    private void changeFragment(VIDEO_TYPE type, int id){
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

        if(mCurrentFragment != null)
            transaction.remove(mCurrentFragment);

        switch (type){
            case Youtube:
                YoutubeFragment f = YoutubeFragment.newInstance();
                f.setUrl("uEki1liieFM");
                f.init();
                mCurrentFragment = f;

                break;
            case BrightCove:
                BrightCoveFragment fragment = BrightCoveFragment.newInstance();
                fragment.setVideoID("5088285407001");
//                fragment.setOnFullScreenListener(mBCOnFullScreenListener);
                mCurrentFragment = fragment;
                break;
        }

        transaction.replace(id, mCurrentFragment);
        transaction.commit();
    }

//    private BrightCoveFragment.OnFullScreenListener mBCOnFullScreenListener = new BrightCoveFragment.OnFullScreenListener(){
//
//        @Override
//        public void fullscreen(boolean state) {
//            mFullScreen = state;
//
//            if(mOnFullscreenListener != null)
//                mOnFullscreenListener.fullscreen(state);
//
//            setFullScreenSize();
//        }
//    };
//
//    private void setFullScreenSize(){
//        RelativeLayout.LayoutParams rlp ;
//        if(mFullScreen){
//            rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//            rlp.width = getContext().getResources().getDisplayMetrics().widthPixels;
//            rlp.height = getContext().getResources().getDisplayMetrics().heightPixels;
//
//            mContentLayout.getLayoutParams().height = 0;
//        }
//        else{
//            rlp = (RelativeLayout.LayoutParams)mFrameLayout.getLayoutParams();
//            rlp.addRule(RelativeLayout.BELOW, R.id.widget_weather);
//            rlp.width = Math.min(getContext().getResources().getDisplayMetrics().widthPixels, getContext().getResources().getDisplayMetrics().heightPixels);
//            rlp.height = rlp.width/16 * 9;
//
//            mContentLayout.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;
//        }
//
//        mFrameLayout.setLayoutParams(rlp);
//    }
}
