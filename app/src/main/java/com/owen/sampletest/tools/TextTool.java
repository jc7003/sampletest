package com.owen.sampletest.tools;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

/**
 * Created by owenchen on 2016/3/7.
 */
public class TextTool {

    private Context mContext;

    public TextTool(Context context){
        mContext = context;
    }

    public int getValueOfTextSizeAttr(int style, int value){
        TypedValue typedValue = new TypedValue();
        mContext.getTheme().resolveAttribute(style, typedValue, true);
        int[] attribute = new int[] { value };
        TypedArray array = mContext.obtainStyledAttributes(typedValue.resourceId, attribute);
        int textSize = array.getDimensionPixelSize(0 /* index */, -1 /* default size */);
        array.recycle();

        return textSize;
    }

    public int autoResize(TextView textView, int limit){
//        L.d("TextTool", "autoResize limit:" + limit);
        int size = (int)textView.getTextSize();
        String word = textView.getText().toString();
        if(word.length() == 0)
            return 0;

        final int offset_limit = 100;

        if(Math.abs(textView.getPaint().measureText(word)-limit) > 25) {
            if (textView.getPaint().measureText(word) < limit) {
                while (textView.getPaint().measureText(word) < limit) {
                    size += ((limit - textView.getPaint().measureText(word)) > offset_limit) ? 10 : 1;
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
                }
            } else {
                while (textView.getPaint().measureText(word) > limit) {
                    size -= ((textView.getPaint().measureText(word) - limit) > offset_limit) ? 10 : 1;
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
                }
            }
        }

        return size;
    }

    private void setLinkClickable(final SpannableStringBuilder clickableHtmlBuilder,
                                  final URLSpan urlSpan, final View.OnClickListener listener) {
        int start = clickableHtmlBuilder.getSpanStart(urlSpan);
        int end = clickableHtmlBuilder.getSpanEnd(urlSpan);
        int flags = clickableHtmlBuilder.getSpanFlags(urlSpan);

        ClickableSpan clickableSpan = new ClickableSpan() {
            public void onClick(View view) {
                view.setTag(urlSpan.getURL());
                listener.onClick(view);
            }
        };
        clickableHtmlBuilder.setSpan(clickableSpan, start, end, flags);
    }


    public CharSequence getClickableHtml(String html, View.OnClickListener listener) {
        Spanned spannedHtml = Html.fromHtml(html);
        SpannableStringBuilder clickableHtmlBuilder = new SpannableStringBuilder(spannedHtml);
        URLSpan[] urls = clickableHtmlBuilder.getSpans(0, spannedHtml.length(), URLSpan.class);
        for(final URLSpan span : urls) {
            setLinkClickable(clickableHtmlBuilder, span, listener);
        }
        return clickableHtmlBuilder;
    }
}
