package com.owen.sampletest.tools;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.GifTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;


/**
 * Created by owenchen on 2015/11/24.
 * use Glide tools, this can auto resize to ImageView
 * case this have imageView type, i use fixCenter type
 */
public class ImageTools {

    private int mWidth, mHeight;
    private RequestManager mRequestManager;
    private RequestListener mRequestListener;
    private Context mContext;

    public interface RequestListener extends com.bumptech.glide.request.RequestListener{};

    public ImageTools(Context context){
        mContext = context;
        mRequestManager =  Glide.with(context);
    }

    public ImageTools(Fragment fragment){
        mContext = fragment.getContext();
        mRequestManager = Glide.with(fragment);
    }

    public void getImageFormURL(String Url, ImageView imageView){
        BitmapTypeRequest bitmapTypeRequest = mRequestManager.load(Url).asBitmap();
        initBitmapTypeRequest(bitmapTypeRequest);

        bitmapTypeRequest.skipMemoryCache(true).into(imageView);
    }

    public void getImageFormURL(String Url, Drawable placeholder, ImageView imageView){
        BitmapTypeRequest bitmapTypeRequest = mRequestManager.load(Url).asBitmap();
        initBitmapTypeRequest(bitmapTypeRequest);
        bitmapTypeRequest.placeholder(placeholder);

        if(mWidth > 0 && mHeight > 0)
            bitmapTypeRequest.override(mWidth,mHeight);

        bitmapTypeRequest.skipMemoryCache(true).centerCrop().into(imageView);
    }

    public void getImageFormURL(String Url, int placeholder, ImageView imageView){
        BitmapTypeRequest bitmapTypeRequest = mRequestManager.load(Url).asBitmap();
        initBitmapTypeRequest(bitmapTypeRequest);
        bitmapTypeRequest.placeholder(placeholder);

        if(mWidth > 0 && mHeight > 0)
            bitmapTypeRequest.override(mWidth,mHeight);

        bitmapTypeRequest.skipMemoryCache(true).centerCrop().into(imageView);
    }

    public void getFitCenterImageFormURL(String Url, int placeholder, ImageView imageView){
        BitmapTypeRequest bitmapTypeRequest = mRequestManager.load(Url).asBitmap();
        initBitmapTypeRequest(bitmapTypeRequest);
        bitmapTypeRequest.placeholder(placeholder);

        if(mWidth > 0 && mHeight > 0)
            bitmapTypeRequest.override(mWidth,mHeight);

        bitmapTypeRequest.skipMemoryCache(true).fitCenter().into(imageView);
    }

    public void getImageFormID(int drawableId, ImageView imageView){
        BitmapTypeRequest bitmapTypeRequest = mRequestManager.load(drawableId).asBitmap();
        initBitmapTypeRequest(bitmapTypeRequest);

        if(mWidth > 0 && mHeight > 0)
            bitmapTypeRequest.override(mWidth,mHeight);

        bitmapTypeRequest.skipMemoryCache(true).centerCrop().into(imageView);
    }

    public void getImageFormID(int drawableId, ImageView imageView, boolean centerCrop){
        BitmapTypeRequest bitmapTypeRequest = mRequestManager.load(drawableId).asBitmap();
        initBitmapTypeRequest(bitmapTypeRequest);

        if(mWidth > 0 && mHeight > 0)
            bitmapTypeRequest.override(mWidth,mHeight);

        if(centerCrop)
            bitmapTypeRequest.skipMemoryCache(true).centerCrop().into(imageView);
        else
            bitmapTypeRequest.skipMemoryCache(true).into(imageView);
    }

    public void getGifImageFormURL(String Url, ImageView imageView){
        GifTypeRequest gifTypeRequest = mRequestManager.load(Url).asGif();
        gifTypeRequest.fitCenter();
        gifTypeRequest.diskCacheStrategy(DiskCacheStrategy.ALL);

        if(mWidth > 0 && mHeight > 0)
            gifTypeRequest.override(mWidth,mHeight);

        gifTypeRequest.skipMemoryCache(true).into(imageView);
    }

    private void initBitmapTypeRequest(BitmapTypeRequest typeRequest){
        typeRequest.fitCenter();
        typeRequest.diskCacheStrategy(DiskCacheStrategy.RESULT);

        if(mWidth > 0 && mHeight > 0)
            typeRequest.override(mWidth,mHeight);

        if(mRequestListener != null)
            typeRequest.listener(mRequestListener);
    }

    public void setRequestListener(RequestListener listener){
        mRequestListener = listener;
    }

    public void resize(int width, int height){
        mWidth = width;
        mHeight = height;
    }

    public void onDestroy(){

        if(mRequestManager!=null) {
            mRequestManager.onLowMemory();
            mRequestManager.onDestroy();
        }

        if(mContext != null)
            Glide.get(mContext).clearMemory();

        clear();
    }

    private void clear(){

        mContext = null;
        mRequestManager = null;
        mRequestListener = null;
        System.gc();
    }

    public void clear(View view){
        Glide.clear(view);

        clear();
    }

    public void clear(List<View> list){
        for(View view : list) {
            view.setTag(null);
            Glide.clear(view);
        }

        clear();
    }
}
