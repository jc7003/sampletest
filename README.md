裡面有一些簡單的sample

環境附上
compileSdkVersion 24
buildToolsVersion "24.0.3"
minSdkVersion 19
targetSdkVersion 24


功能介紹
主頁![Screenshot_1493985839.png](https://bitbucket.org/repo/aEARky/images/2592521343-Screenshot_1493985839.png)


CreateViewByJsonActivity 頁面
分析json檔案(default.json)並做成現
![Screenshot_1493984155.png](https://bitbucket.org/repo/aEARky/images/4009888011-Screenshot_1493984155.png)

SearchActivity頁面
簡單使用 search 元件


PickNumbersActivity 頁面
實作類似選擇時鐘功能，但選項是地點
![Screenshot_1493984116.png](https://bitbucket.org/repo/aEARky/images/4159248529-Screenshot_1493984116.png)

![Screenshot_1493984122.png](https://bitbucket.org/repo/aEARky/images/1110247893-Screenshot_1493984122.png)

HyperLinkActivity 頁面
針對特殊字做link處理

ViewpagerActivity 頁面
簡單的換頁交錯使用sdk

BtmNavigationActivity 頁面
玩最近看到的新東西
![
](https://bitbucket.org/repo/aEARky/images/2822567152-Screenshot_1493985932.png)

ripple按鈕
製作水波圖樣
![Screenshot_1493984146.png](https://bitbucket.org/repo/aEARky/images/3485412521-Screenshot_1493984146.png)![
](https://bitbucket.org/repo/aEARky/images/793791486-Screenshot_1493986065.png)